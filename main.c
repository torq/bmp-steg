#include <stdlib.h>

#include "defs.h"
#include "steg.h"
#include "utils.h"

int main(int argc, char** argv) {
    int command = get_cmd(argc, argv);
    switch(command) {
        case CMD_STEG:
            steg(argv[2], argv[3], argv[4]);
            break;
        case CMD_UNSTEG:
            unsteg(argv[2], argv[3]);
            break;
        default:
            print_usage();
            exit(ERR_USAGE);
    }

    return 0;
}
