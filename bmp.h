#ifndef STEG_BMP
#define STEG_BMP

#include <stdio.h>

typedef struct {
    unsigned char magic[2];
    unsigned int offset, height, width, padding;
} bmp_metadata;

void get_bmp_metadata(FILE*, bmp_metadata*);
int is_bmp(bmp_metadata*);

#endif
