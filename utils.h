#ifndef STEG_UTILS
#define STEG_UTILS

#include <stdio.h>

int get_cmd(int , char** );
void print_usage();

FILE* open_important_file(char* , char* );
long get_file_size(FILE *file);

unsigned read4Bytes(FILE* );

#endif
