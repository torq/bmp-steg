#include <string.h>
#include <stdlib.h>

#include "defs.h"
#include "utils.h"

int get_cmd(int argc, char** argv) {
    if (argc > 2) {
        if (!strcmp(argv[1], "steg") && argc >= 5) 
            return CMD_STEG;
        else if (!strcmp(argv[1], "unsteg") && argc >= 4)
            return CMD_UNSTEG;
    }
    
    return INC_CMD;
}

void print_usage() {
    printf("Usage:\n");
    printf("\t./<exec name> steg <bmp filepath> <message filepath> <output filepath>\n");
    printf("\t./<exec name> unsteg <bmp filepath> <output filepath>\n");
}

FILE* open_important_file(char* path, char* mode) {
    FILE* file = fopen(path, mode);
    if (!file)
        exit(ERR_FILE);
    return file;
}

long get_file_size(FILE *file) {
    fseek (file, 0, SEEK_END);
    long length = ftell (file);
    fseek (file, 0, SEEK_SET);
    return length;
}

// read little-endian 4 bytes and transforms it into a big-endian int
unsigned read4Bytes(FILE* arq) {
    unsigned value = 0;
    unsigned char byte;

    int i, j;
    for (i=0,j=0; i<4; i++, j+=8) {
        byte = fgetc(arq);
        value |= ((int)byte) << j;
    }

    return value;
}