#ifndef STEG_DEFS
#define STEG_DEFS

#define TRUE  1
#define FALSE 0

// error when trying to open a file
#define ERR_FILE    1001

// error when trying to verify if a given file is indeed a BMP file
#define ERR_BMP     1002

// error of the usage of the program
#define ERR_USAGE   1003

#define INC_CMD    0
#define CMD_STEG   1
#define CMD_UNSTEG 2

#endif
