#include <stdlib.h>

#include "bmp.h"
#include "defs.h"
#include "utils.h"

void get_bmp_metadata(FILE* arq, bmp_metadata *metadata) {
    // ensures the pointer to be in the beginning of the file
    fseek(arq, 0, SEEK_SET);

    metadata->magic[0] = fgetc(arq);
    metadata->magic[1] = fgetc(arq);

    fseek(arq,8,SEEK_CUR);
    metadata->offset = read4Bytes(arq);

    fseek(arq,4,SEEK_CUR);
    metadata->width = read4Bytes(arq);
    metadata->height = read4Bytes(arq);

    metadata->padding = (metadata->width*3)%4;

    // ensures the pointer to be back in the beginning of the file
    fseek(arq, 0, SEEK_SET);
}

int is_bmp(bmp_metadata* metadata) {
    return metadata->magic[0] == 'B' && metadata->magic[1] == 'M';
}