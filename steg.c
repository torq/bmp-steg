#include "steg.h"

#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "defs.h"
#include "utils.h"

static FILE* open_bmp(char* bmp_path, bmp_metadata *metadata);
static void unsteg_core(FILE *in, bmp_metadata* const metadata, FILE *out);

void unsteg(char* bmp_path, char* out_path) {
    // open the files
    bmp_metadata metadata;
    FILE *arq = open_bmp(bmp_path, &metadata);
    FILE *output = open_important_file(out_path, "w");

    // jumps "offset" pixels to get to the pixel array
    fseek(arq, metadata.offset, SEEK_SET);

    // extract the message and print it into the file
    unsteg_core(arq, &metadata, output);

    // end program (cleaning)
    fclose(arq);
    fclose(output);
}

void steg(char* orig_path, char* msg_path, char* out_path) {
    // open files
    bmp_metadata metadata;
    FILE *origImg = open_bmp(orig_path, &metadata);
    FILE *msg = open_important_file(msg_path, "r"); // TODO: Add msg validations
    FILE *stegImg = open_important_file(out_path, "wb");

    // copiar original para a saída até chegar em offset
    for (int i=0; i<metadata.offset; ++i) {
        char c = fgetc(origImg);
        fputc(c, stegImg);
    }

    // iterar mensagem e imagem original, produzindo imagem esteganografada
    // até chegar no tamanho da mensagem
    char msg_char;
    while((msg_char = fgetc(msg)) != EOF) {
        unsigned char index = 0x01;
        for (int j=0; j<8; ++j, index <<= 1) {
            char new_character = fgetc(origImg);
            if (msg_char & index)
                new_character |= 0x1;
            else
                new_character &= 0xfe;

            fputc(new_character, stegImg);
        }
    }

    // codificar o \0
    for (int j=0; j<8; ++j)
        fputc(fgetc(origImg) & 0xfe, stegImg);

    // copiar o restante da original para a saída até chegar no final do arquivo
    while(!feof(origImg))
        fputc(fgetc(origImg), stegImg);

    // end program (cleaning)
    fclose(origImg);
    fclose(msg);
    fclose(stegImg);
}

static FILE* open_bmp(char* bmp_path, bmp_metadata *metadata) {
    FILE *bmp_file = open_important_file(bmp_path, "rb");
    
    // extract the metadata from a supposedly BMP file
    get_bmp_metadata(bmp_file, metadata);
    if (!is_bmp(metadata))
        exit(ERR_BMP);
    
    return bmp_file;
}

static void unsteg_core(FILE *in, bmp_metadata* const metadata, FILE *out) {
    while (TRUE) {
        unsigned char message = 0;
        // extract the character
        for (int i=0, j=0; i<8; i++, j++) {
            // padding logic
            if (metadata->padding && j == (metadata->width * 3)) {
                fseek(in, metadata->padding, SEEK_CUR);
                j=0;
            }

            unsigned char byte = fgetc(in); // gets the pixel
            byte &= 0x01; // gets its LSB
            byte <<= i; // makes the LSB the i-th bit of the message
            message |= byte;
        }

        if (!message)
            break;
        fputc(message, out);
    };
}