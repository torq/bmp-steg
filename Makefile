CC=gcc
OPTIONS=-std=c99 -g
OUT=steg

UNSTEG_CMD=unsteg
IMG_STEG=./example/testImage.bmp
OUTPUT=./example/output

all:
	$(CC) $(OPTIONS) *.c -o $(OUT)

uns: all
	./$(OUT) $(UNSTEG_CMD) $(IMG_STEG) $(OUTPUT)
	cat $(OUTPUT)

clean:
	rm $(OUT)
