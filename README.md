# bmp_steg

The objective of this program is to extract the information contained inside a BMP file, when given information has been hidden using the steganography technique called LSB (more on it [here](http://numaboa.com.br/criptografia/cripto-papers/56-lsb "LSB - Técnica dos bits menos significativos") (portuguese)).

The program extracts the message inside a given BMP file and returns the content inside [output.txt](https://github.com/VictorQRS/bmp_steg/blob/master/resposta.txt).

## Uso

To use it, it will be necessary to compile the program first (`> ` means that these commands should be run inside a shell):

`> make`

Once compiled, you will be able to run using the following command:

`> ./steg <path to the BMP file> <path to the output file>`

For example, `> ./steg testImage.bmp output`

The entire process might generate some files. To clean them out, you just need to:

`> make clean`
